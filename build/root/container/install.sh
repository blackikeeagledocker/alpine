#!/bin/sh

set -e

alpineversion=$1
if [ -z "$alpineversion" ]; then
    alpineversion=edge
fi

enabletesting=$2

if [ "$alpineversion" = "edge" ] && [ "$enabletesting" -eq 1 ]; then
    echo "https://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
fi

apk add --update tini su-exec

/container/upgrade.sh

/container/user-cleanup.sh

