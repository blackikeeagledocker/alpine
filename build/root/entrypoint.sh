#!/bin/sh

/container/user-setup.sh

if [[ -e /container/prepare-command.sh ]]; then
    sh /container/prepare-command.sh
fi

if [[ 0 = `id -u` ]]; then
    exec tini -g -- su-exec $C_USER:$C_USER "$@"
else
    exec tini -g -- "$@"
fi
